import traverson from 'traverson-promise'
import traversonHal from 'traverson-hal'

traverson.registerMediaType(traversonHal.mediaType, traversonHal)

export default traverson
