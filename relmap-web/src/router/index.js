import Vue from 'vue'
import VueRouter from 'vue-router'
import RMap from '@/components/map/r-map'
import RAdmin from '@/components/admin/r-admin'

Vue.use(VueRouter)

export default new VueRouter({
    routes: [
        {
            path: '/map',
            name: 'r-map',
            component: RMap
        },
        {
            path: '/admin',
            name: 'r-admin',
            component: RAdmin
        }
    ]
})
