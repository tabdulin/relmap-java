import Person from "@/entities/Person";

export default class Missionary {
    constructor () {
        this._id = null
        this._confession = null
        this._person = new Person()
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get confession() {
        return this._confession;
    }

    set confession(value) {
        this._confession = value;
    }

    get person() {
        return this._person;
    }

    set person(value) {
        this._person = value;
    }
}


