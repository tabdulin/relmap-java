import Address from '@/entities/Address'

export default class Person {
    constructor() {
        this._id = null;
        this._name = null;
        this._registrationAddress = new Address();
        this._actualAddress = new Address();
        this._contacts = null;
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get registrationAddress() {
        return this._registrationAddress;
    }

    set registrationAddress(value) {
        this._registrationAddress = value;
    }

    get actualAddress() {
        return this._actualAddress;
    }

    set actualAddress(value) {
        this._actualAddress = value;
    }

    get contacts() {
        return this._contacts;
    }

    set contacts(value) {
        this._contacts = value;
    }
}
