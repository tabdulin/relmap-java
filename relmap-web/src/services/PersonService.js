import AbstractService from './AbstractService'
import AddressService from './AddressService'

export default new class PersonService extends AbstractService {
    constructor () {
        super ('persons')
    }

    async save (person) {
        if (!person) {
            return Promise.reject('Null person')
        }

        const savedRegistrationAddress = await AddressService.save(person.registrationAddress)
        person.registrationAddress = savedRegistrationAddress
        const savedActualAddress = await AddressService.save(person.actualAddress)
        person.actualAddress = savedActualAddress
        person.type = 'izzi.relmap.model.Person'
        return super.save(person)
    }
}
