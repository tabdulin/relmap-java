import AbstractService from './AbstractService'
import PersonService from './PersonService'

export default new class MissionaryService extends AbstractService {
    constructor () {
        super ('missionaries')
    }

    async save (missionary) {
        if (!missionary) {
            return Promise.reject("Null missionary")
        }

        let savedPerson = await PersonService.save(missionary.person)
        missionary.person = savedPerson
        return super.save(missionary)
    }

}
