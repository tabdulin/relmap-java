import traverson from '@/utils/traverson'

export default class AbstractService {
    constructor (resources) {
        // TODO: move host to profile settings (dev, production, etc)
        this._host = 'http://localhost:8080'
        this._resources = resources
        this._api = `${this._host}/api/${this._resources}`
        this._search = `${this._api}/search`
    }


    get resources() {
        return this._resources;
    }

    get host() {
        return this._host;
    }

    get api() {
        return this._api;
    }

    get search() {
        return this._search;
    }

    async loadAll () {
        console.debug(`Loading all ${this.resources}`)
        var items = await traverson
            .from(this.api)
            .jsonHal()
            .follow(`${this.resources}[$all]`)
            .getResource()
            .result
        console.debug(`Loaded ${this.resources}`, items)
        return items
    }


    async save (item) {
        console.debug(`Saving ${this.resources} item`, item)
        if (!item) {
            return Promise.reject('Null item')
        }

        let saved
        if (item.id) {
            console.debug('Editing existing item')
            saved = await traverson
                .from(`${this.api}/${item.id}`)
                .json()
                .convertResponseToObject()
                .patch(item)
                .result
        } else {
            console.debug('Saving new item')
            saved = await traverson
                .from(`${this.api}`)
                .json()
                .convertResponseToObject()
                .post(item)
                .result
        }

        console.debug(`Saved ${this.resources} item`, saved)
        return saved;
    }

    delete (item) {
        if (!item) {
            return Promise.reject('Null item')
        }

        return traverson
            .from(`${this.api}/${item.id}`)
            .delete()
            .result
    }
}
