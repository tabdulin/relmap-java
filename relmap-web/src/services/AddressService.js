import traverson from '@/utils/traverson'
import AbstractService from './AbstractService'

export default new class AddressService extends  AbstractService {
    constructor() {
        super ('addresses')
    }

    async loadByTitle (title) {
        console.debug(`Loading addresses by title ${title}`)
        const addresses =  await traverson
            .from(this.search)
            .jsonHal()
            .follow('titles')
            .withTemplateParameters({
                title: title
            })
            .follow(`${this.resources}[$all]`)
            .getResource()
            .result
        console.debug('Loaded addresses', addresses)
        return addresses
    }
}
