package izzi.relmap.dao;

import izzi.relmap.model.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@RepositoryRestResource(excerptProjection = Address.FullProjection.class)
public interface AddressRepository extends CrudRepository<Address, Long> {

    @RestResource(path = "titles", rel = "titles")
    List<Address> findByTitleContainingIgnoreCase(String title);
}
