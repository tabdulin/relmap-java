package izzi.relmap.dao;

import izzi.relmap.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.EntityGraph;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

public class PersonRepositoryExtensionImpl implements PersonRepositoryExtension<Person> {
    @Autowired
    private EntityManager em;

    @Override
    @EntityGraph("graph.Person")
    @Transactional
    public Person save(Person person) {
        em.persist(person);
        return person;
    }
}
