package izzi.relmap.dao;

public interface PersonRepositoryExtension<T> {
    <S extends T> S save(S person);
}
