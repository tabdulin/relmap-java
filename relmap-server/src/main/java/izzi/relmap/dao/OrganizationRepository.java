package izzi.relmap.dao;

import izzi.relmap.model.Organization;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource(excerptProjection = Organization.FullProjection.class)
public interface OrganizationRepository extends CrudRepository<Organization, Long> {
}
