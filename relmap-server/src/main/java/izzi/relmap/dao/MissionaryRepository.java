package izzi.relmap.dao;

import izzi.relmap.model.Missionary;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;


@RepositoryRestResource(excerptProjection = Missionary.FullProjection.class)
public interface MissionaryRepository extends CrudRepository<Missionary, Long> {
    @Override
    @EntityGraph(attributePaths = {
            "person",
            "person.registrationAddress",
            "person.actualAddress"
    })
    List<Missionary> findAll();

    @Override
    @EntityGraph(attributePaths = {
            "person",
            "person.registrationAddress",
            "person.actualAddress"
    })
    Optional<Missionary> findById(Long id);

    @Override
    @Modifying
    @Transactional
    @EntityGraph(attributePaths = {
            "person",
            "person.registrationAddress",
            "person.actualAddress"
    })
    Missionary save(Missionary missionary);
}

