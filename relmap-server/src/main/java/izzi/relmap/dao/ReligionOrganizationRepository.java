package izzi.relmap.dao;

import izzi.relmap.model.ReligionOrganization;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@RepositoryRestResource(path = "religion-organizations",
        collectionResourceRel = "religion-organizations",
        itemResourceRel = "religion-organization",
        excerptProjection = ReligionOrganization.FullProjection.class)
public interface ReligionOrganizationRepository extends CrudRepository<ReligionOrganization, Long> {

    @Override
    @EntityGraph(attributePaths = {
            "organization",
            "organization.person"
    })
    Iterable<ReligionOrganization> findAll();

    @Override
    @Modifying
    @Transactional
    @EntityGraph(attributePaths = {
            "organization",
            "organization.person"
    })
    ReligionOrganization save(ReligionOrganization religionOrganization);
}
