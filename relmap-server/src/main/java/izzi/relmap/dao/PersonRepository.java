package izzi.relmap.dao;

import izzi.relmap.model.Person;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@RepositoryRestResource(excerptProjection = Person.FullProjection.class)
public interface PersonRepository extends CrudRepository<Person, Long> {//, PersonRepositoryExtension<Person> {
    @Override
    @Modifying
    @Transactional
    @EntityGraph(attributePaths = {"registrationAddress", "actualAddress"})
    Person save(Person person);


    @RestResource(path = "names", rel="names")
    @EntityGraph(attributePaths = {"registrationAddress", "actualAddress"})
    List<Person> findByNameContainingIgnoreCase(@Param("name") String name);

    @Override
    @EntityGraph(attributePaths = {"registrationAddress", "actualAddress"})
    Iterable<Person> findAll();
}
