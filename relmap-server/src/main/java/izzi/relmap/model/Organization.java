package izzi.relmap.model;

import lombok.Data;
import org.springframework.data.rest.core.config.Projection;

import javax.persistence.*;

@Data
@Entity
public class Organization extends AbstractItemEntity {
    @Column
    private String title;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "head_person_id")
    private Person head;

    @Projection(types = Organization.class)
    public interface FullProjection extends AbstractEntity.FullProjection {
        String getTitle();
        Person.FullProjection getHeadPerson();
    }
}
