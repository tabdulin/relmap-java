package izzi.relmap.model;

import lombok.Data;
import org.hibernate.annotations.Proxy;
import org.springframework.data.rest.core.config.Projection;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Data
@Entity
@Proxy(lazy = false)
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Address extends AbstractEntity {
    @Column
    private String title;
    @Column
    private BigDecimal longitude;
    @Column
    private BigDecimal latitude;

    @Projection(name = "AddressFullProjection", types = Address.class)
    public interface FullProjection extends AbstractEntity.FullProjection {
        String getTitle();
        BigDecimal getLatitude();
        BigDecimal getLongitude();
    }
}
