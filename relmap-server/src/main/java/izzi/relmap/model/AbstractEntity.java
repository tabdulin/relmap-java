package izzi.relmap.model;

import lombok.Data;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Data
@MappedSuperclass
public abstract class AbstractEntity implements Serializable  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "creation_date")
    @Generated(GenerationTime.INSERT)
    protected Date creationDate;

    @Column(name = "modification_date")
    @Generated(GenerationTime.ALWAYS)
    protected Date modificationDate;

    public interface FullProjection {
        Long getId();
        Date getCreationDate();
        Date getModificationDate();
    }

}
