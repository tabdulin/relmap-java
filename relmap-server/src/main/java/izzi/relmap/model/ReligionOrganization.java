package izzi.relmap.model;

import lombok.Data;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.config.Projection;

import javax.persistence.*;

@Data
@Entity
public class ReligionOrganization extends AbstractEntity {
    @Column
    private String confession;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organization_id")
    @RestResource(exported = false)
    private Organization organization;

    @Projection(name = "FullReligionOrganization", types = ReligionOrganization.class)
    public interface FullProjection extends AbstractEntity.FullProjection {
        String getConfession();
        Organization.FullProjection getOrganization();
    }
}
