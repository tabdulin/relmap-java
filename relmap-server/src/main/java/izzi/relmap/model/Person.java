package izzi.relmap.model;

import lombok.Data;
import org.springframework.data.rest.core.config.Projection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;

@Data
@Entity
public class Person extends AbstractItemEntity {
    @Column
    private String name;

    @Projection(name = "FullPerson", types = {Person.class})
    public interface FullProjection extends AbstractItemEntity.FullProjection {
        Long getId();
        String getName();
    }
}
