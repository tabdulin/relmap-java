package izzi.relmap.model;

import lombok.Data;
import org.springframework.data.rest.core.config.Projection;

import javax.persistence.*;

@Data
@Entity
public class Missionary extends AbstractEntity {
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id")
//    @RestResource(exported = false)
    private Person person;

    private String confession;

    @Projection(name= "FullMissionary", types = {Missionary.class})
    public interface FullProjection extends AbstractEntity.FullProjection {
        Person.FullProjection getPerson();
        String getConfession();
    }
}
