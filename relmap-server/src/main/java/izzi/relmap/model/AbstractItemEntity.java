package izzi.relmap.model;

import lombok.Data;

import javax.persistence.*;

@Data
@MappedSuperclass
public abstract class AbstractItemEntity extends AbstractEntity {
    @Column
    private String contacts;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "registration_address_id")
    private Address registrationAddress;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "actual_address_id")
    private Address actualAddress;

    public interface FullProjection extends AbstractEntity.FullProjection {
        String getContacts();
        Address.FullProjection getRegistrationAddress();
        Address.FullProjection getActualAddress();
    }
}
