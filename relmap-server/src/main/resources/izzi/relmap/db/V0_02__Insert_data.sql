
insert into address (id, title) values(1, 'person 1 registration address');
insert into address (id, title) values(2, 'person 1 actual address');
insert into person (id, name, registration_address_id, actual_address_id) values (1, 'person', 1, 2);

insert into address (id, title) values(3, 'organization 1 address');
insert into organization (id, title, registration_address_id, head_person_id) values (1, 'organization 1', 3, 1);

--insert into missionary (id, person_id, confession) values(1, 1, 'confession');
--insert into religion_organization(id, organization_id, confession) values(1, 1, 'confession');

alter sequence address_id_seq restart with 1000;
alter sequence person_id_seq restart with 1000;
alter sequence organization_id_seq restart with 1000;
alter sequence missionary_id_seq restart with 1000;
alter sequence religion_organization_id_seq restart with 1000;