create table address (
  id serial primary key,
  creation_date timestamp not null default now(),
  modification_date timestamp not null default now(),

  title text not null unique,
  longitude decimal(9, 6),
  latitude decimal(9, 6),

  constraint unique_coordinates unique (longitude, latitude)
);

create table person (
  id serial primary key,
  creation_date timestamp not null default now(),
  modification_date timestamp not null default now(),

  name varchar(512) not null,
  contacts varchar(1024),
  registration_address_id integer references address,
  actual_address_id integer references address
);

create table organization (
  id serial primary key,
  creation_date timestamp not null default now(),
  modification_date timestamp not null default now(),

  title varchar(1024) not null,
  contacts varchar(1024),
  registration_address_id integer references address,
  actual_address_id integer references address,
  head_person_id integer references person
);

create table missionary (
  id serial primary key,
  creation_date timestamp not null default now(),
  modification_date timestamp not null default now(),

  person_id integer unique not null references person,
  confession varchar(256) not null
);

create table religion_organization (
  id serial primary key,
  creation_date timestamp not null default now(),
  modification_date timestamp not null default now(),

  organization_id integer unique not null references organization,
  confession varchar(256) not null
)