package izzi.relmap.dao;

import io.restassured.internal.http.Status;
import izzi.relmap.model.Address;
import izzi.relmap.model.Person;
import org.junit.Test;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.TypeReferences;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static org.junit.Assert.*;
import static org.junit.Assert.*;
import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

public class PersonRepositoryTest extends AbstractRepositoryTest {
    @Override
    protected String getResourcesURI() {
        return "/api/persons";
    }

    @Test
    public void saveNew() {
        Person person = createRandomPerson("Person");
        personRepository.save(person);
    }

    @Test
    public void restSaveNew() {
        Person person = createRandomPerson("Person");

        given()
                .contentType("application/hal+json")
                .body(person)
        .when()
                .post(getResourcesURI())
                .prettyPeek()
        .then()
                .statusCode(HttpStatus.CREATED.value());
    }

    @Test
    public void saveNewWithoutAddresses() {
        Person person = createRandomPerson("Person");
        person.setActualAddress(null);
        person.setRegistrationAddress(null);
        Person saved = personRepository.save(person);
        assertNotNull(saved.getId());
    }

    @Test
    public void restSaveNewWithoutAddresses() {
        Person person = createRandomPerson("Person");
        person.setActualAddress(null);
        person.setRegistrationAddress(null);
        ResponseEntity<Person> response = restTemplate.postForEntity(getResourcesURI(), person, Person.class);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody().getId());
    }

    @Test
    public void restSaveNewWithExistingAddresses() {
        Person person = createRandomPerson("Person");

        Address actualAddress = createRandomAddressAndSave("Person actual address");
        Address registrationAddress = createRandomAddressAndSave("Person registration address");
        person.setActualAddress(actualAddress);
        person.setRegistrationAddress(registrationAddress);

        ResponseEntity<Person> response = restTemplate.postForEntity(getResourcesURI(), person, Person.class);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody().getId());
        assertNotNull(response.getBody().getActualAddress().getId());
        assertNotNull(response.getBody().getRegistrationAddress().getId());
    }

    @Test
    public void restFindById() {
        Person person = createRandomPersonAndSave("Person to be found by id");

        ResponseEntity<Person> response = restTemplate.getForEntity(getResourceURI(person.getId()), Person.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody().getId());
    }


    @Test
    public void restFindByNameContaining() {
        createRandomPersonAndSave("Person to be found by name");
        String uri = UriComponentsBuilder.fromPath(getSearchURI("names"))
                .queryParam("name", "name")
                .build()
                .encode()
                .toUriString();
        assertEquals("/api/persons/search/names?name=name", uri);

        RequestEntity request = RequestEntity.get(URI.create(uri)).accept(MediaTypes.HAL_JSON).build();

        ResponseEntity<Resources<Person>> response = restTemplate.exchange(request, new TypeReferences.ResourcesType<Person>(){});
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(response.getBody().getContent().size() > 0);
    }

    @Test
    public void restSaveExistingPersonWithAnotherExistingAddress() {
        Person person = createRandomPersonAndSave("Existing person");
        Address registrationAddress = createRandomAddressAndSave("Another registration address");
        person.setRegistrationAddress(registrationAddress);
        person.setName("Changed " + person.getName());
        Person updated = restTemplate.patchForObject(getResourceURI(person.getId()), person, Person.class);

        assertEquals(person.getId(), updated.getId());
    }

}