package izzi.relmap.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.internal.mapping.Jackson2Mapper;
import io.restassured.mapper.factory.Jackson2ObjectMapperFactory;
import izzi.relmap.model.Address;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.TypeReferences;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Collection;

import static org.junit.Assert.*;
import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;


public class AddressRepositoryTest extends AbstractRepositoryTest {

    @Override
    protected String getResourcesURI() {
        return "/api/addresses";
    }

    @Test
    public void restSaveNew() {
        Address address = createRandomAddress("Address to be save");

        given()
                .contentType("application/hal+json")
                .body(address)
        .post(getResourcesURI())
                .prettyPeek()
        .then()
                .statusCode(HttpStatus.CREATED.value());
    }

    @Test
    public void restFindById() {
        Address address = createRandomAddressAndSave("Address to be found by id");

        get(getResourceURI(address.getId()))
                .prettyPeek()
        .then()
                .body("id", equalTo(address.getId().intValue()));
    }

    @Test
    public void restFindAll() {
        Address address = createRandomAddressAndSave("Address to be found by all");

        URI uri = UriComponentsBuilder.fromPath(getResourcesURI()).build().toUri();
        RequestEntity request = RequestEntity.get(uri).accept(MediaTypes.HAL_JSON).build();
        ResponseEntity<Resources<Address>> response = restTemplate.exchange(request, new TypeReferences.ResourcesType<Address>(){});
        assertEquals(HttpStatus.OK, response.getStatusCode());

        Collection<Address> addresses = response.getBody().getContent();
        boolean contains = false;
        for (Address a : addresses) {
            if (address.getId().equals(a.getId())) {
                contains = true;
            }
        }
        assertTrue(contains);
    }

    @Test
    public void restSaveExisting() {
        Address address = createRandomAddressAndSave("Address to be updated");
        address.setTitle("Updated " + address.getTitle());

        given()
                .body(address)
        .when()
                .put(getResourceURI(address.getId()))
                .prettyPeek()
        .then()
                .statusCode(HttpStatus.OK.value());

/*
        Address response = restTemplate.patchForObject(getResourceURI(address.getId()), address, Address.class);
        assertNotNull(response);
*/
    }

}