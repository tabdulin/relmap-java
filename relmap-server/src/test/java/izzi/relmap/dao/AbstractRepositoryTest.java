package izzi.relmap.dao;

import io.restassured.RestAssured;
import izzi.relmap.model.AbstractEntity;
import izzi.relmap.model.Address;
import izzi.relmap.model.Missionary;
import izzi.relmap.model.Person;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.UUID;

// TODO: set up running on fresh docker database
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractRepositoryTest<T extends AbstractEntity> {
    @Autowired
    protected TestRestTemplate restTemplate;

    @Autowired
    protected AddressRepository addressRepository;

    @Autowired
    protected PersonRepository personRepository;

    @LocalServerPort
    protected Integer port;

    @PostConstruct
    public void setUp() {
        restTemplate.getRestTemplate().setRequestFactory(new OkHttp3ClientHttpRequestFactory());
        RestAssured.port = port;
    }

    protected Address createRandomAddress(String title) {
        Address address = new Address();
        address.setTitle(title + " " + UUID.randomUUID());
        address.setLatitude(BigDecimal.valueOf(Math.random()));
        address.setLongitude(BigDecimal.valueOf(Math.random()));
        return address;
    }

    protected Address createRandomAddressAndSave(String title) {
        return addressRepository.save(createRandomAddress(title));
    }

    protected Person createRandomPerson(String name) {
        Person person = new Person();
        person.setName(name + " " + UUID.randomUUID());
        person.setContacts("Contacts " + UUID.randomUUID());
        person.setActualAddress(createRandomAddressAndSave("Person actual address"));
        person.setRegistrationAddress(createRandomAddressAndSave("Person registration address"));
        return person;
    }

    protected Person createRandomPersonAndSave(String name) {
        Person person = createRandomPerson(name);
        person.setActualAddress(createRandomAddressAndSave("Person actual address"));
        person.setRegistrationAddress(createRandomAddressAndSave("Person registration address"));
        return personRepository.save(person);
    }

    protected Missionary createRandomMissionary() {
        Missionary missionary = new Missionary();
        missionary.setConfession("Missionary confession " + UUID.randomUUID());
        missionary.setPerson(createRandomPerson("Missionary person"));
        return missionary;
    }

    protected abstract String getResourcesURI();

    protected String getResourceURI(Long id) {
        return getResourcesURI() + "/" + id;
    }

    protected String getSearchURI(String search) {
        return getResourcesURI() + "/search/" + search;
    }

}
