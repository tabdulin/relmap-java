package izzi.relmap.dao;

import izzi.relmap.model.Missionary;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class MissionaryRepositoryTest extends AbstractRepositoryTest {

    @Override
    protected String getResourcesURI() {
        return "/api/missionaries";
    }

    @Test
    public void restSaveNewWithExistingPerson() {
        Missionary m = createRandomMissionary();
        m.setPerson(createRandomPersonAndSave("Missionary person to be saved"));
        ResponseEntity<Missionary> response = restTemplate.postForEntity(getResourcesURI(), m, Missionary.class);
        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
        Assert.assertNotNull(response.getBody().getId());
    }
}